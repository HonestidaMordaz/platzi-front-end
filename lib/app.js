var $ = window.jQuery
var key = 'f58c62920e4b94271958a9c010d33e07'
var url = 'http://gateway.marvel.com:80/v1/public/series?title=avengers&apikey=f58c62920e4b94271958a9c010d33e07'

Promise
	.resolve($.get(url))
	.then(function (results) {
		var characters = results.data.results[0].characters.item
		var promises = []

		for (var i in characters) {
			var character = characters[i]
			var charactersURI = character.resourceURI + '?' + key

			promises.push(Promise.resolve($.get(characterURI)))
		}

		return Promise.all(promises)
	})
	.then(function (characters) {
		console.log(characters)
	})
	.catch(function (err) {
		console.error(err)
	})